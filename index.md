---
home: true
heroImage: /logo.png
actionText: 快速开始 →
actionLink: /guide/introduction

altActionText: 下载源码
altActionLink: https://gitee.com/open-source-byte/source-vue

features:
  - title: 💡 主流技术栈
    details: 基于Java（Spring Boot）、Vue（uniApp）等主流技术开发
  - title: ⚡️ 多端打包
    details: 快速打包交付，完美支持微信小程序、H5、Android和IOS
  - title: 🛠️ 极速开发
    details: 高效率开发，使用代码生成器可以一键生成前后端代码
  - title: 📦 数据可视化
    details: 大屏视觉绚丽，复杂报表一键导出
  - title: 🔩 消息中心
    details: 基于Websocket实现消息通讯，支持短信、邮件推送
  - title: 🔑 社区生态
    details: 众多开源项目总有适合你的，在线文档，学习社区，交流群
footer: MIT Licensed |  Copyright © 2022-2032 开源字节 Open Source Byte All Rights Reserved
---
