# 准备工作

```java
JDK >= 1.8 (推荐1.8版本)
Mysql >= 5.7.0 (推荐5.7版本)
Redis >= 3.0
Maven >= 3.0
Node >= 12
```

## 后端部署

> 前往 Gitee 下载页面(https://gitee.com/open-source-byte/source-vue (opens new window))下载解压到工作目录

1、导入到 Eclipse/Idea，菜单 File -> Import，然后选择 Maven -> Existing Maven Projects，点击 Next> 按钮，选择工作目录，然后点击 Finish 按钮，即可成功导入。 Eclipse 会自动加载 Maven 依赖包，初次加载会比较慢（根据自身网络情况而定）

2、创建数据库 source-vue 并导入 doc 目录下的数据脚本 source-vue.sql

3、将 doc 目录下的 yml 配置文件放到 source-admin 中的 resources 目录下

4、打开项目运行 cn.source.RuoYiApplication.java，控制台出现(♥◠‿◠)ﾉﾞ 开源字节(https://sourcebyte.vip) ლ(´ڡ`ლ)ﾞ表示启动成功

## 前端部署

> 前往 Gitee 下载页面(https://gitee.com/open-source-byte/source-vue-ui (opens new window))下载解压到工作目录

```js
# 安装依赖
npm install

# 强烈建议不要用直接使用 cnpm 安装，会有各种诡异的 bug，可以通过重新指定 registry 来解决 npm 安装速度慢的问题。
npm install --registry=https://registry.npmmirror.com

# 本地开发 启动项目
npm run dev
```

打开浏览器，输入：(http://localhost:80 (opens new window)) 默认账户/密码 admin/???）若能正确展示登录页面，并能成功登录，菜单及页面展示正常，则表明环境搭建成功

建议使用 Git 克隆，因为克隆的方式可以和 开源字节 随时保持更新同步。

::: tip 温馨提醒

1 、前端安装完 node 后，最好设置下淘宝的镜像源，不建议使用 cnpm（可能会出现奇怪的问题）；

2、因为本项目是前后端完全分离的，所以需要前后端都单独启动好，才能进行访问；

3、微信搜索 **【开源字节】** 公众号，回复<span style="color:red"> “ 管理员密码 ” </span>，获取密码；

:::

## Uniapp 部署

> 前往 Gitee 下载页面(https://gitee.com/cookieBoy/house (opens new window))下载解压到工作目录

- **浏览器运行** ：进入 house-life 项目，点击工具栏的运行 -> 运行到浏览器 -> 选择浏览器，即可在浏览器里面体验 uni-app 的 H5 版

- **真机运行** ：连接手机，开启 USB 调试，进入 house 项目，点击工具栏的运行 -> 真机运行 -> 选择运行的设备，即可在该设备里面体验 uni-app

如 `手机无法识别`，请点击菜单运行-运行到手机或模拟器-真机运行常见故障排查指南。 注意目前开发 App 也需要安装微信开发者工具

- **在微信开发者工具里运行** ：进入 house 项目，点击工具栏的运行 -> 运行到小程序模拟器 -> 微信开发者工具，即可在微信开发者工具里面体验 uni-app

## 工具下载

::: tip 都是作者平时使用的一些实用软件

这里整理了一些软件工具的下载包，方便大家下载使用。微信搜索 【开源字节】 公众号，回复“ 软件工具 ”，获取下载链接；

:::

## IDE 插件

**1、idea 好用的插件**

如果您使用的后端 IDE 是 idea(推荐)的话，可以安装以下工具来提高开发效率及代码格式化

- Chinese 汉化
- google format 格式化
- save action 保存即格式化
- Mybatis x Mybatis 增强
- Mybatis Log 自动拼接 sql 和参数
- Rainbow Brackets 彩虹括号
- lombok 用注解的方式,简化了 JavaBean 的编写,避免了冗余和样板式代码

**2、vscode 好用的插件**

如果您使用的前端 IDE 是 vscode(推荐)的话，可以安装以下工具来提高开发效率及代码格式化

- Chinese 汉化
- Eclipse Keymap 快捷键
- Vetur - vue 开发必备 （也可以选择 Volar）
- ESLint - 脚本代码检查
- Prettier - 代码格式化
- Stylelint - css 格式化
- open in browser 快速在浏览器中打开 html 文件
