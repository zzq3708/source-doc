# 介绍

## 项目简介

[开源字节](https://gitee.com/open-source-byte/source-vue) 是一套全部开源的快速开发平台，毫无保留给个人及企业免费使用。

它可以帮助你快速搭建多端项目（PC 端，移动端），该项目使用主流的技术栈，相信不管是从学习新技术使用还是其他方面，都能帮助到你。

- 移动端采用 Vue、Uniapp、Uview。
- PC 端采用 Vue、Element UI。
- 后端采用 Spring Boot、Mybatis、Spring Security、Redis & Jwt。
- 使用 Camunda 引擎、bpmn.js 设计器实现工作流(待完善)。
- 使用 Websocket 实现即时通讯。
- 使用 OSS、COS 实现对象存储。
- 使用 Luckysheet 实现 Excel 拖拽赋值的 Web 数据录入。
- 使用 Vxe-table 实现单行编辑，即时保存效果。
- 使用 ECharts,UCharts 实现数据可视化图表。
- 使用 DataV 展示可视化大屏数据。
- 使用 IReport 实现企业级 Web 报表。
- 使用 kkFileView 实现在线预览，支持 doc,docx,Excel,pdf,txt,zip,rar,图片等
- 使用 OAuth2 实现三方应用授权 。
- 支持多种登录方式（扫码登录，验证码登录，密码登录）
- 支持加载动态权限菜单，控制菜单权限，按钮权限，数据权限。
- 高效率开发，使用代码生成器可以一键生成前后端代码。

## 项目体验

演示地址：http://boot.sourcebyte.vip

开发文档：http://doc.sourcebyte.vip

官网地址：https://sourcebyte.vip

## 开发文档

开发文档地址为 [开源字节 开发文档](https://gitee.com/open-source-byte/source-doc)，采用 Vitepress 开发。如发现文档有误，欢迎提 pr 帮助我们改进。

如需本地运行文档，请拉取代码到本地。

```bash
# 拉取代码
git clone https://gitee.com/open-source-byte/source-doc.git

# 安装依赖
npm install

# 运行项目
npm run dev
```

## 基础知识

本项目需要一定前端基础知识，请确保掌握 Vue/uniApp 的基础知识，以便能处理一些常见的问题。

建议在开发前先学一下以下内容，提前了解和学习这些知识，会对项目理解非常有帮助:

- [Vue 官网](https://cn.vuejs.org/)
- [uniApp 官网](https://uniapp.dcloud.io/)
- [uView](https://www.uviewui.com/)
- [Es6](https://es6.ruanyifeng.com/)
- [Less](https://less.bootcss.com)

## 作者信息

- 姓名：詹 Sir
- 微信：18720989281
- 邮箱：261648947@qq.com

## 加入我们

- [开源字节](https://sourcebyte.vip) 还在持续更新中，本项目欢迎您的参与，共同维护。
- 如果你想加入我们，可以多提供一些好的建议或者提交 pr，我们会根据你的活跃度邀请你加入。
